using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life_Script : MonoBehaviour
{
    private Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, 5);
    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, -15);
    }
}
