using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_2_v2_Script : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Transform _transform;
    public GameObject Bala_Enemiga_lv_2;

    private float Spawm_time = 0.35f;
    private int vida = 3;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();
        Destroy(this.gameObject, 5);
    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(15, -20);
        //Debug.Log("Time=" + Spawm_time);
        Spawm_time += Time.deltaTime;
        if (Spawm_time >= 0.35f)
        {
            var BulletPosition = new Vector3(_transform.position.x, _transform.position.y + -2, _transform.position.z);
            Instantiate(Bala_Enemiga_lv_2, BulletPosition, Quaternion.identity);
            Spawm_time = 0;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("vida= " + vida);
        if (collision.gameObject.layer == 3)
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.layer == 7)
        {
            vida--;
            if (vida <= 0)
            {
                Game_C_Script.Xp++;
                Game_C_Script.score++;
                Destroy(this.gameObject);
            }
        }
        if (collision.gameObject.layer == 8)
        {
            vida -= 2;
            if (vida <= 0)
            {
                Game_C_Script.Xp++;
                Game_C_Script.score++;
                Destroy(this.gameObject);
            }
        }
    }
}