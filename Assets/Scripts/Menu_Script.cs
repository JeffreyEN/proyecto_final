using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu_Script : MonoBehaviour
{
    public Text Pulsar;
    public AudioSource moneda;
    private float Pulsar_time;
    private float Parpadear_time;
    private float inicio_time;
    public bool inicio = false;
    private float Par_in = 1;
    private float Par_out = 2;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Pulsar.gameObject.SetActive(false);
        Pulsar_time += Time.deltaTime;
        Debug.Log(Pulsar_time);
        if (Pulsar_time >=5)
        {
            Pulsar.gameObject.SetActive(true);
            Parpadear_time += Time.deltaTime;
            if (Parpadear_time <= Par_in)
                Pulsar.gameObject.SetActive(true);
            else if (Parpadear_time <= Par_out)
                Pulsar.gameObject.SetActive(false);
            else
                Parpadear_time = 0;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Par_in = Par_in * 0.2f;
                Par_out = Par_out * 0.2f ;
                moneda.Play();
                inicio = true;
            }
        }
        if(inicio)
            inicio_time += Time.deltaTime;
        if (inicio_time >= 2.5)
            SceneManager.LoadScene("Scene_1");

    }
}
