using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteriode_M_script : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;

    public Pj_scripts pj_Scripts;

    private int vida = 4;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        pj_Scripts = FindObjectOfType<Pj_scripts>();
    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, -Random.Range(2f, 6f));
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("vida= "+vida);
        if (collision.gameObject.layer == 3)
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.layer == 7)
        {
            vida--;
            if(vida<=0){
                pj_Scripts.Xp++;
                Destroy(this.gameObject);
            }
        }
        if (collision.gameObject.layer == 8)
        {
            vida-=2;
            if (vida <= 0)
            {
                pj_Scripts.Xp++;
                Destroy(this.gameObject);
            }
        }
    }
}
