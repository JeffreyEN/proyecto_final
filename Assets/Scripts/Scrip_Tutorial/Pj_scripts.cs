using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pj_scripts : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    private Transform _transform;
    private Animator _animator;
    private Color originalColor;

    public GameObject Bala_lv_1;
    public GameObject Bala_lv_2;
    public GameObject Portal;
    public Text Txt_Lv;
    public Text Txt_Tuto;
    public Text Txt_Vida;
    public Text Txt_Xp;
    public Text Txt_Dash;

    private int WalkSpeed = 10;
    private float Sprint_time = 10;
    private float cooldown_time = 0;
    private float cooldown_Bullet = 1;
    private float tutorial_time = 0;
    private float portal_time = 0;
    private float Da�o_time = 0;
    private float Da�o_time_D = 0;

    //private float Reinicio_time = 0;
    private bool moverse = true;
    public bool Reiniciar = false;

    public int Xp = 0;
    public float Lv = 1;
    public int vida = 11;
    private bool B_puerta = false;
    public bool Da�o_Nave = false;


    private AudioSource sdn_Laser;
    public AudioSource Snd_Portal;
    public AudioSource Perder_Vida;

    public Sprite Nave_1;
    public Sprite Nave_2;
    public Sprite Nave_3;
    public Sprite Nave_4;
    public Sprite Nave_5;



    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        _transform = GetComponent<Transform>();
        _animator = GetComponent<Animator>();
        originalColor = sr.color;
        sdn_Laser = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        _animator.SetInteger("Estado", 0);
        Txt_Tuto.gameObject.SetActive(false);
        tutorial_time += Time.deltaTime;
        //Debug.Log("time= "+ tutorial_time);
        //Debug.Log("time= " + portal_time);

 
        if (moverse)
        {
            Controles();
        }
        else
            rb2d.velocity = new Vector2(0, 0);
        Tutorial();
        Cambio_Naves();
        if (portal_time >= 2)
            SceneManager.LoadScene("Scene_2");
        if (B_puerta == true)
        {
            _animator.SetInteger("Estado", 1);
            portal_time += Time.deltaTime;
        }
        if (vida <= 1)
        {
            Txt_Tuto.text = "PRESIONA SPACE PARA REINICIAR EL JUEGO";
            Txt_Tuto.gameObject.SetActive(true);
            sr.enabled = false;
            rb2d.velocity = new Vector2(0, 0);
            if (Input.GetKey(KeyCode.Space))
                SceneManager.LoadScene("Scene_0");
        }
        Parpadear();




    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 6)
        {
            Destroy(collision.gameObject);
            vida--;
            Perder_Vida.Play();
            Da�o_Nave = true;


        }
        if (collision.gameObject.layer == 9)
        {
            moverse = false;
            Snd_Portal.Play();
            B_puerta = true;
            sr.enabled = false;
            Da�o_Nave = true;

        }
    }
    private void Controles()
    {
        cooldown_Bullet -= Time.deltaTime;
        Sprint_time += Time.deltaTime;
        //Debug.Log("bullet= " + cooldown_Bullet);
        //Debug.Log("cooldown_time = " + cooldown_time);
        //Debug.Log("Sprint_time = " + Sprint_time);
        //movimientos 
        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX = false;
            rb2d.velocity = new Vector2(WalkSpeed, rb2d.velocity.y);
            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, -WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }


                }
            }
            else
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
            }// no se vaya en una direci�n despues de soltar la tecla
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            rb2d.velocity = new Vector2(-WalkSpeed, rb2d.velocity.y);
            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }

            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, -WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }

            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }
                }

            }
            else
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
            }// no se vaya en una direci�n despues de soltar la tecla
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, WalkSpeed);
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb2d.velocity = new Vector2(WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }

                }
            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, -WalkSpeed);
            if (Input.GetKey(KeyCode.RightArrow))
            {
                sr.flipX = false;
                rb2d.velocity = new Vector2(WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb2d.velocity = new Vector2(-WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }

                }
            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
        }
        else
        {
            // detener la nave 
            rb2d.velocity = new Vector2(0, 0);
            rb2d.velocity = new Vector2(0, 0);
        }
        if (Input.GetKeyDown(KeyCode.X))


        {
            if (Lv == 1)
            {
                if (cooldown_Bullet <= -1)
                {
                    sdn_Laser.Play();
                    var BulletPosition = new Vector3(_transform.position.x, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;
                }

            }
            else if (Lv == 2)
            {
                if (cooldown_Bullet <= -1)
                {
                    sdn_Laser.Play();
                    var BulletPosition_1 = new Vector3(_transform.position.x + 0.3f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_1, Quaternion.Euler(new Vector3(0, 0, 90)));

                    var BulletPosition_2 = new Vector3(_transform.position.x - 0.3f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_2, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;
                }
            }
            else if (Lv == 3)
            {
                if (cooldown_Bullet <= -0.8)
                {
                    sdn_Laser.Play();
                    var BulletPosition_1 = new Vector3(_transform.position.x, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_2, BulletPosition_1, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;

                }
            }
            else if (Lv >= 4)
            {
                if (cooldown_Bullet <= -0.8)
                {
                    sdn_Laser.Play();
                    var BulletPosition_1 = new Vector3(_transform.position.x, _transform.position.y + 1.5f, _transform.position.z); ;
                    Instantiate(Bala_lv_2, BulletPosition_1, Quaternion.Euler(new Vector3(0, 0, 90)));

                    var BulletPosition_2 = new Vector3(_transform.position.x - 0.5f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_2, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;

                    var BulletPosition_3 = new Vector3(_transform.position.x + 0.5f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_3, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;
                }
            }
        }
    }
    private void Tutorial()
    {
        if (Xp >= 11)
        {
            Lv++;
            Txt_Lv.text = "Lv " + Lv;
            Xp = 0;
        }
        if (tutorial_time >= 3)
        {
            Txt_Tuto.gameObject.SetActive(true);
        }
        if (tutorial_time >= 10)
        {
            Txt_Tuto.gameObject.SetActive(false);
        }
        if (tutorial_time >= 13)
        {
            Txt_Tuto.text = "Dispara con X";
            Txt_Tuto.gameObject.SetActive(true);
        }
        if (tutorial_time >= 17)
        {
            Txt_Tuto.gameObject.SetActive(false);
        }
        if (tutorial_time >= 20)
        {
            Txt_Tuto.text = "Usa tu Dash con Z";
            Txt_Tuto.gameObject.SetActive(true);
        }
        if (tutorial_time >= 23)
        {
            Txt_Tuto.gameObject.SetActive(false);
        }
        if (tutorial_time >= 25)
        {
            Txt_Vida.gameObject.SetActive(true);
        }
        if (tutorial_time >= 29)
        {
            Txt_Vida.gameObject.SetActive(false);
        }
        if (tutorial_time >= 30)
        {
            Txt_Xp.gameObject.SetActive(true);
        }
        if (tutorial_time >= 34)
        {
            Txt_Xp.gameObject.SetActive(false);
        }
        if (tutorial_time >= 35)
        {
            Txt_Dash.gameObject.SetActive(true);
        }
        if (tutorial_time >= 40)
        {
            Txt_Dash.gameObject.SetActive(false);
        }
        if (tutorial_time >= 41)
        {
            Txt_Tuto.fontSize = 50;
            Txt_Tuto.text = "space travel";
            Txt_Tuto.gameObject.SetActive(true);
        }
        if (tutorial_time >= 50)
        {
            Txt_Tuto.gameObject.SetActive(false);
        }
        if (tutorial_time >= 60)
        {
            Portal.SetActive(true);
        }
    }
    private void Cambio_Naves()
    {
        if (Lv == 1)
        {
            sr.sprite = Nave_1;
        }
        if (Lv == 2)
        {
            sr.sprite = Nave_2;
        }
        if (Lv == 3)
        {
            sr.sprite = Nave_3;
        }
        if (Lv == 4)
        {
            sr.sprite = Nave_4;
        }
        if (Lv == 5)
        {
            sr.sprite = Nave_5;
        }
    }
    private void Parpadear()
    {
            if (Da�o_Nave == true)
            {
                Da�o_time += Time.deltaTime;
                Da�o_time_D += Time.deltaTime;
                if (Da�o_time <= 0.2f)
                {
                    sr.enabled = false;
                }
                else if (Da�o_time <= 0.4f)
                    sr.enabled = true;
                else
                    Da�o_time = 0;
                if (Da�o_time_D >= 3)
                {
                    Da�o_Nave = false;
                    Da�o_time_D = 0;
                    sr.enabled = true;
                }
            }
        }
}