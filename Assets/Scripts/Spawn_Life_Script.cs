using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Life_Script : MonoBehaviour
{
    private Transform _transform;

    public GameObject Life;
    private float Spawm_time = 0;
    private float elejido;
    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Spawm_time += Time.deltaTime;
        if (Spawm_time >= 20f)
        {
            elejido = Random.Range(0,3);
            if (elejido >= 1)
            {
                var BulletPosition = new Vector3(_transform.position.x + Random.Range(-7.7f, 7.7f), _transform.position.y + -3f, _transform.position.z);
                Instantiate(Life, BulletPosition, Quaternion.Euler(new Vector3(0, 0, 0)));

            }
            Spawm_time = 0;
        }
    }
}
