using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pj_scripts_v2 : MonoBehaviour
{
    public Game_C_Script Game_C_Script;

    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    private Transform _transform;
    private Animator _animator;
    private CircleCollider2D _CircleCollider2D;
    //private Color originalColor;

    public GameObject Bala_lv_1;
    public GameObject Bala_lv_2;
    public GameObject Portal;
    public Text Txt_Lv;
    public Text Txt_Tuto;
    public Text Txt_Vida;
    public Text Txt_Xp;
    public Text Txt_Dash;
    public Text Txt_Score;

    private int WalkSpeed = 10;
    private int Score;
    private float Sprint_time = 10;
    private float cooldown_time = 0;
    private float cooldown_Bullet = 1;
    private float Game_time = 0;
    private float portal_time = 0;
    private float Da�o_time = 0;
    private float Da�o_time_D = 0;

    //private float Reinicio_time = 0;
    private bool moverse = true;
    public bool Reiniciar = false;
    public bool Da�o_Nave = false;


    public int Xp ;
    public float Lv ;
    public float Lv_Maps;
    public int vida ;
    private bool B_puerta = false;


    public AudioSource Snd_Portal;
    private AudioSource sdn_Laser;
    public AudioSource Perder_Vida_snd;
    public AudioSource Morir_snd;

    public Sprite Nave_1;
    public Sprite Nave_2;
    public Sprite Nave_3;
    public Sprite Nave_4;
    public Sprite Nave_5;



    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        _transform = GetComponent<Transform>();
        _animator = GetComponent<Animator>();
        _CircleCollider2D = GetComponent<CircleCollider2D>();
        //originalColor = sr.color;
        sdn_Laser = GetComponent<AudioSource>();
        Game_C_Script = GetComponent<Game_C_Script>();
    }

    // Update is called once per frame
    void Update()
    {
        Xp = Game_C_Script.Xp;
        vida = Game_C_Script.vida;
        Lv = Game_C_Script.Lv;
        Lv_Maps = Game_C_Script.Lv_Maps;

        Txt_Score.gameObject.SetActive(true);
        Txt_Score.text = ("SCORE: " + Game_C_Script.score);
        _animator.SetInteger("Estado", 0);
        Txt_Tuto.gameObject.SetActive(false);
        Game_time += Time.deltaTime;
        //Debug.Log("time= "+ tutorial_time);
        Debug.Log("da�o:"+ Da�o_Nave);


        if (moverse)
        {
            Controles();
        }
        else
            rb2d.velocity = new Vector2(0, 0);
        //Tutorial();
        Cambio_Naves();
        if (portal_time >= 2)
        {
            if(Lv_Maps ==2)
            SceneManager.LoadScene("Scene_3");
        }

        if (B_puerta == true)
        {
            _animator.SetInteger("Estado", 1);
            portal_time += Time.deltaTime;
        }
        if (Game_C_Script.vida <= 0)
        {
           Morir_snd.Play();
            Txt_Tuto.text = "PRESIONA SPACE PARA REINICIAR EL JUEGO";
            Txt_Tuto.gameObject.SetActive(true);
            sr.enabled = false;
            rb2d.velocity = new Vector2(0, 0);
            if (Input.GetKey(KeyCode.Space))
                SceneManager.LoadScene("Scene_0");
        }
        if (Xp >= 11)
        {
            Game_C_Script.Lv++;
            Txt_Lv.text = "Lv " + Game_C_Script.Lv;
            Game_C_Script.Xp = 0;
        }
        Parpadear();
        Abrir_Portal();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 6)
        {
            Destroy(collision.gameObject);
            Game_C_Script.vida--;
            Da�o_Nave = true;
            Perder_Vida_snd.Play();
        }
        if (collision.gameObject.layer == 9)
        {
            moverse = false;
            Snd_Portal.Play();
            B_puerta = true;
            sr.enabled = false;
            Game_C_Script.Lv_Maps++;
            Da�o_Nave = true;
            Perder_Vida_snd.Play();
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 10)
        {
            Destroy(other.gameObject);
            Game_C_Script.vida--;
            Da�o_Nave = true;
            Perder_Vida_snd.Play();
        }
        if (other.gameObject.layer == 11)
        {
            Destroy(other.gameObject);
            Game_C_Script.vida = 11;
        }
    }
    private void Controles()
    {
        cooldown_Bullet -= Time.deltaTime;
        Sprint_time += Time.deltaTime;
        //Debug.Log("bullet= " + cooldown_Bullet);
        //Debug.Log("cooldown_time = " + cooldown_time);
        //Debug.Log("Sprint_time = " + Sprint_time);
        //movimientos 
        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX = false;
            rb2d.velocity = new Vector2(WalkSpeed, rb2d.velocity.y);
            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, -WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }


                }
            }
            else
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
            }// no se vaya en una direci�n despues de soltar la tecla
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            rb2d.velocity = new Vector2(-WalkSpeed, rb2d.velocity.y);
            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }

            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, -WalkSpeed);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }

            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }
                }

            }
            else
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
            }// no se vaya en una direci�n despues de soltar la tecla
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, WalkSpeed);
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb2d.velocity = new Vector2(WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }

                }
            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, -WalkSpeed);
            if (Input.GetKey(KeyCode.RightArrow))
            {
                sr.flipX = false;
                rb2d.velocity = new Vector2(WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb2d.velocity = new Vector2(-WalkSpeed, rb2d.velocity.y);
                if (Sprint_time >= 9.9)
                    if (Input.GetKey(KeyCode.Z))
                    {
                        cooldown_time += Time.deltaTime;
                        if (cooldown_time <= 0.1)
                        {
                            rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                        }
                        else
                        {
                            cooldown_time = 0;
                            Sprint_time = 0;
                        }
                    }
            }
            else if (Input.GetKey(KeyCode.Z))
            {
                if (Sprint_time >= 9.9)
                {
                    cooldown_time += Time.deltaTime;
                    if (cooldown_time <= 0.1)
                    {
                        rb2d.velocity = new Vector2(rb2d.velocity.x * 5, rb2d.velocity.y * 5);
                    }
                    else
                    {
                        cooldown_time = 0;
                        Sprint_time = 0;
                    }

                }
            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
        }
        else
        {
            // detener la nave 
            rb2d.velocity = new Vector2(0, 0);
            rb2d.velocity = new Vector2(0, 0);
        }
        if (Input.GetKeyDown(KeyCode.X))


        {
            if (Lv == 1)
            {
                if (cooldown_Bullet <= -1)
                {
                    sdn_Laser.Play();
                    var BulletPosition = new Vector3(_transform.position.x, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;
                }

            }
            else if (Lv == 2)
            {
                if (cooldown_Bullet <= -1)
                {
                    sdn_Laser.Play();
                    var BulletPosition_1 = new Vector3(_transform.position.x + 0.3f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_1, Quaternion.Euler(new Vector3(0, 0, 90)));

                    var BulletPosition_2 = new Vector3(_transform.position.x - 0.3f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_2, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;
                }
            }
            else if (Lv == 3)
            {
                if (cooldown_Bullet <= -0.8)
                {
                    sdn_Laser.Play();
                    var BulletPosition_1 = new Vector3(_transform.position.x, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_2, BulletPosition_1, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;

                }
            }
            else if (Lv == 4)
            {
                if (cooldown_Bullet <= -0.8)
                {
                    sdn_Laser.Play();
                    var BulletPosition_1 = new Vector3(_transform.position.x, _transform.position.y + 1.5f, _transform.position.z); ;
                    Instantiate(Bala_lv_2, BulletPosition_1, Quaternion.Euler(new Vector3(0, 0, 90)));

                    var BulletPosition_2 = new Vector3(_transform.position.x - 0.5f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_2, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;

                    var BulletPosition_3 = new Vector3(_transform.position.x + 0.5f, _transform.position.y + 1.5f, _transform.position.z);
                    Instantiate(Bala_lv_1, BulletPosition_3, Quaternion.Euler(new Vector3(0, 0, 90)));
                    cooldown_Bullet = 0;
                }
            }
        }
    }
    private void Cambio_Naves()
    {
        if (Lv == 1)
        {
            sr.sprite = Nave_1;
        }
        if (Lv == 2)
        {
            sr.sprite = Nave_2;
        }
        if (Lv == 3)
        {
            sr.sprite = Nave_3;
        }
        if (Lv == 4)
        {
            sr.sprite = Nave_4;
        }
        if (Lv == 5)
        {
            sr.sprite = Nave_5;
        }
    }
    private void Parpadear()
    {
        if (Da�o_Nave == true)
        {
            Da�o_time += Time.deltaTime;
            Da�o_time_D += Time.deltaTime;
            if (Da�o_time <= 0.2f)
            {
                sr.enabled = false;
            }
            else if (Da�o_time <= 0.4f)
                sr.enabled = true;
            else
                Da�o_time = 0;
            if (Da�o_time_D >= 3)
            {
                Da�o_Nave = false;
                Da�o_time_D = 0;
                sr.enabled = true;
                _CircleCollider2D.enabled = true;
            }
            else
            {
                _CircleCollider2D.enabled = false;
            }

        }

    }
    private void Abrir_Portal()
    {
        if (Game_time >= 60)
        {
            Portal.SetActive(true);
        }
    }
}