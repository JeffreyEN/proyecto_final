using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barra_Vida_v2_Script : MonoBehaviour
{
    private Animator _animator;

    public Pj_scripts_v2 pj_Scripts_V2;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        pj_Scripts_V2 = FindObjectOfType<Pj_scripts_v2>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (pj_Scripts_V2.vida)
        {
            case 0:
                _animator.SetInteger("Estado", 0);
                break;
            case 1:
                _animator.SetInteger("Estado", 1);
                break;
            case 2:
                _animator.SetInteger("Estado", 2);
                break;
            case 3:
                _animator.SetInteger("Estado", 3);
                break;
            case 4:
                _animator.SetInteger("Estado", 4);
                break;
            case 5:
                _animator.SetInteger("Estado", 5);
                break;
            case 6:
                _animator.SetInteger("Estado", 6);
                break;
            case 7:
                _animator.SetInteger("Estado", 7);
                break;
            case 8:
                _animator.SetInteger("Estado", 8);
                break;
            case 9:
                _animator.SetInteger("Estado", 9);
                break;
            case 10:
                _animator.SetInteger("Estado", 10);
                break;
            case 11:
                _animator.SetInteger("Estado", 11);
                break;
        }
    }
}
