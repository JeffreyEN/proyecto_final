using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteriode_S_v2_script : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;

    public Pj_scripts_v2 pj_Scripts_V2;

    private int vida = 2;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        pj_Scripts_V2 = FindObjectOfType<Pj_scripts_v2>();
        Destroy(this.gameObject, 5);
    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, -Random.Range(4f, 10f));
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("vida= " + vida);
        if (collision.gameObject.layer == 3)
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.layer == 7)
        {
            vida--;
            if (vida <= 0)
            {
                Game_C_Script.Xp++;
                Game_C_Script.score++;
                Destroy(this.gameObject);
            }
        }
        if (collision.gameObject.layer == 8)
        {
            vida -= 2;
            if (vida <= 0)
            {
                Game_C_Script.Xp++;
                Game_C_Script.score++;
                Destroy(this.gameObject);
            }
        }
    }
}
