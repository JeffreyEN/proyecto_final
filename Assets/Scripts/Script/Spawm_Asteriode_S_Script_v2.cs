using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawm_Asteriode_S_Script_v2 : MonoBehaviour
{
    private Transform _transform;

    public GameObject Asteriode_S;

    private float Spawm_time = 0;

    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Spawm_time += Time.deltaTime;
        if (Spawm_time >= Random.Range(4f, 6f))
        {
            var BulletPosition = new Vector3(_transform.position.x + Random.Range(-9.5f, 9.5f), _transform.position.y + -3f, _transform.position.z);
            Instantiate(Asteriode_S, BulletPosition, Quaternion.identity);
            Spawm_time = 0;
        }
    }
}
