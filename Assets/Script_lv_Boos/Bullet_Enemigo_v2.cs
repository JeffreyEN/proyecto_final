using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Enemigo_v2 : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        Destroy(this.gameObject, 5);

    }
    // Update is called once per frame
    void Update()
    {

        rb2d.velocity = new Vector2(rb2d.velocity.x, -40);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 3)
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.layer == 6)
        {
            Destroy(this.gameObject);
        }
    }
}
