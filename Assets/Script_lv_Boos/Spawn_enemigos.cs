using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_enemigos : MonoBehaviour
{
    private Transform _transform;

    public GameObject Enemigo_1;
    public GameObject Enemigo_2;
    public GameObject Enemigo_3;

    private float Spawm_time = 0;
    private float elejido;

    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Spawm_time += Time.deltaTime;
        if (Spawm_time >= Random.Range(4f, 14f))
        {
            elejido = Random.Range(0f, 3f);
            if (elejido<=1) {
                var BulletPosition = new Vector3(_transform.position.x + Random.Range(-7.7f, 7.7f), _transform.position.y + -2.3f, _transform.position.z);
                Instantiate(Enemigo_1, BulletPosition, Quaternion.Euler(new Vector3(0, 0, 180)));
                Spawm_time = 0;
            }
            if (elejido <= 2)
            {
                var BulletPosition = new Vector3(_transform.position.x -7.7f, _transform.position.y + -2.3f, _transform.position.z);
                Instantiate(Enemigo_2, BulletPosition, Quaternion.Euler(new Vector3(0, 0, 180)));
                Spawm_time = 0;
            }
            if (elejido <= 3)
            {
                var BulletPosition = new Vector3(_transform.position.x +7.7f, _transform.position.y + -2.3f, _transform.position.z);
                Instantiate(Enemigo_3, BulletPosition, Quaternion.Euler(new Vector3(0, 0, 180)));
                Spawm_time = 0;
            }
        }
    }
}
